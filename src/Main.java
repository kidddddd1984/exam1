/**
 * Created by liuchang on 16/5/9.
 */

import com.liuchang.Exam1;

import java.util.Calendar;

public class Main {
    public static void main(String[] args) {
        // timer
        long begin;
        long end;

        //================================================

        // Test number
        begin = Calendar.getInstance().getTimeInMillis();
        Exam1 number = Exam1.parse("134297381", (f, s) -> s > f);
        end = Calendar.getInstance().getTimeInMillis();
        System.out.print("Number Test Start: \n");
        String _num = number.getNextLongest();
        while (_num != null) {
            System.out.print("sub: " + _num + "\n");
            _num = number.getNextLongest();
        }
        System.out.print("Number Test finished in " + (end - begin) + "ms\n\n\n");

        //=====================================================

        // Test binary
        begin = Calendar.getInstance().getTimeInMillis();
        Exam1 binary = Exam1.parse("011011110110000001010111111111010", (f, s) -> s == f);
        end = Calendar.getInstance().getTimeInMillis();
        System.out.print("Binary Test Start: \n");
        String _bin = binary.getNextLongest();
        while (_bin != null) {
            System.out.print("sub: " + _bin + "\n");
            _bin = binary.getNextLongest();
        }
        System.out.print("Binary Test finished in " + (end - begin) + "ms\n\n\n");

        //=====================================================

        // Test binary
        begin = Calendar.getInstance().getTimeInMillis();
        Exam1 alphabet = Exam1.parse("aaBBBcCddDDeeBcDDDDDDcccccc", (f, s) -> s == f);
        end = Calendar.getInstance().getTimeInMillis();
        System.out.print("Alphabet Test Start: \n");
        String _alpha = alphabet.getNextLongest();
        while (_alpha != null) {
            System.out.print("sub: " + _alpha + "\n");
            _alpha = alphabet.getNextLongest();
        }
        System.out.print("Alphabet Test finished in " + (end - begin) + "ms\n\n\n");
    }

}
