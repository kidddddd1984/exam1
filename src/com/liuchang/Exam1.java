package com.liuchang;

import java.util.Stack;
import java.util.Arrays;

public class Exam1 {

    /**
     * CompareFunc is a functional interface
     * take two elements then test the condition
     */
    @FunctionalInterface
    public interface CompareFunc {
        boolean run(char f, char s);
    }

    /**
     * Pair be used to save the indexes which take the beginning and the end of a substring
     */
    private class Pair<U> {
        U first;
        U second;

        Pair(U f, U s) {
            first = f;
            second = s;
        }
    }

    private final char[] m_s;  // input array
    private int m_max;  // length of the longest substring
    private CompareFunc m_func;  // compare function
    private Stack<Pair<Integer>> m_sets;  // indexes set of substring

    /**
     * Factory entry
     * @param s
     * @param func
     * @return Exam1
     */
    public static Exam1 parse(String s, CompareFunc func){
        Exam1 _temp = new Exam1(s);
        _temp.m_func = func;

        // parse input
        _temp.run();

        return _temp;
    }

    private Exam1(String s) {
        m_s = s.toCharArray();
        m_max = 0;
        m_sets = new Stack<>();

    }

    /**
     * Get longest arrays
     */
    public String getNextLongest() {
        Pair<Integer> _pair;
        if (!m_sets.isEmpty()) {
            _pair = m_sets.pop();
            if ((_pair.second - _pair.first) == m_max)
                return String.valueOf(Arrays.copyOfRange(m_s, _pair.first, _pair.second));
            return null;
        }
        return null;
    }

    private void run() {
        int _head = 0;  // pointer to the first element of compare pair
        int _tail = 1;  // pointer to the second element of compare pair

        int _current = 0;  // pointer to the head of current substring

        // traversing input array
        while (_tail < m_s.length) {
            // check neighbour elements
            boolean _ret = m_func.run(m_s[_head], m_s[_tail]);

            // when the pair is a inflection point
            if (!_ret) {
                // get substring length
                int _length = _tail - _current;
                // only longer will be push into stack
                if (_length >= m_max) {
                    m_sets.push(new Pair<>(_current, _tail));
                    m_max = _length;
                }
                // refresh current pointer
                _current = _tail;
            }
            // pointer increase
            _head++;
            _tail++;
        }
        // when traversing over
        // check the last substring
        int _length = _tail - _current;
        if (_length >= m_max) {
            m_sets.push(new Pair<>(_current, _tail));
            m_max = _length;
        }


    }
}
