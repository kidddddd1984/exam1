# 質問 1-1～1-3
全部は文字列を処理するため，factory modeを採用することにしました．`com.liuchang.Exam1`はfactory classです．
```java
public static Exam1 parse(String s, CompareFunc func);
```
で文字列`s`を持ち，処理関数を`func`に設定したExam1型instaceを作ってくれます．その結果は
```java
public String getNextLongest();
```
でもらえます．


`Main`ではせたら，Outputに結果が出ます．それぞれに「Test number」「Test binary」「Test alphabet」で分けています．

# 質問 1-4
上の分析したように，共通点はみんな全部定数型の文字列を操作対象にして、文字ずつ比較アクションを行うことです．アクションを分離すれば，残した部分はほぼ同じです．
更に，アルファベットならば，ASCIIで`unsigned int`型と同じなので、アクションもシンプルな値比較になります．関数型プログラミングに向いてますので，ここはJava８で導入したlambda式の出番となります（匿名クラスと関数型インターフェイスの話はここで不問にしましょう）．
なお，今回の問題は並行（Concurrent）あるいは並列計算にも向いてます．

質問自体を少し離れて見れば，もし入力は文字列じゃなくて，任意のArrayならば，今回の質問はもっと抽象的になれます．ジェネリクスを用いて、typeと機能の完全分離ができます．
実際，最初はこの方法で試したが，Javaのジェネリクスはtype eraseの特性がありますので，完全にCやC#見たいなやり方では通りません．
更にprimitive typeとObjectの互い変換するとき，autoboxing/unboxingの作業はパファーマンスに影響が出る恐れがありますので，諦めました．

Javaに関しては今回は初めて触れたなので、おそらく間違っているところがたくさんあります．
どうぞよろしくお願いします．